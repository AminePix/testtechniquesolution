package TestAmineExceciceTechnique.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ouasmine on 27/11/2017.
 */
public class Direction {

    public static final Direction directionNorth = new Direction(0,1);
    public static final Direction directionSouth = new Direction(0,-1);
    public static final Direction directionEast = new Direction(1,0);
    public static final Direction directionWest = new Direction(-1,0);
    public static final List<Direction> volant;

    static {
        volant =  new ArrayList<>();
        volant.add(directionNorth);
        volant.add(directionWest);
        volant.add(directionSouth);
        volant.add(directionEast);
    }

    private int coordonneeX;
    private int coordonneeY;

    public Direction(int coordonneeX, int coordonneeY) {
        this.coordonneeX = coordonneeX;
        this.coordonneeY = coordonneeY;
    }

    public int getCoordonneeX() {
        return coordonneeX;
    }

    public void setCoordonneeX(int coordonneeX) {
        this.coordonneeX = coordonneeX;
    }

    public int getCoordonneeY() {
        return coordonneeY;
    }

    public void setCoordonneeY(int coordonneeY) {
        this.coordonneeY = coordonneeY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Direction direction = (Direction) o;

        if (coordonneeX != direction.coordonneeX) return false;
        return coordonneeY == direction.coordonneeY;
    }

    @Override
    public int hashCode() {
        int result = coordonneeX;
        result = 31 * result + coordonneeY;
        return result;
    }
}
