package TestAmineExceciceTechnique.Model;

/**
 * Created by Ouasmine on 27/11/2017.
 */
public class Tondeuse {

    private int positionX;
    private int positionY;
    private Direction directionActuelle;

    public Tondeuse() {}

    public Tondeuse(int positionX, int positionY, Direction directionActuelle) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.directionActuelle = directionActuelle;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public Direction getDirectionActuelle() {
        return directionActuelle;
    }

    public void setDirectionActuelle(Direction directionActuelle) {
        this.directionActuelle = directionActuelle;
    }

    private void changeDirection(char orientation){
        int index;
        switch (orientation){
            case 'G':
                index = Direction.volant.indexOf(this.directionActuelle);
                index = index + 1;
                if(index == 4)
                    index = 0;
                this.directionActuelle = Direction.volant.get(index);
                break;
            case 'D':
                index = Direction.volant.indexOf(this.directionActuelle);
                index = index - 1;
                if(index == -1)
                    index = 3;
                this.directionActuelle = Direction.volant.get(index);
                break;
        }
    }

    private void avance(){
        this.positionX += directionActuelle.getCoordonneeX();
        this.positionY += directionActuelle.getCoordonneeY();
    }

    public void executeInstructions(String instructions){
        int n = instructions.length();
        for(int i=0; i<n ;i++){
            char cmd = instructions.charAt(i);
            switch (cmd){
                case 'G':
                case 'D':
                    changeDirection(cmd);
                    break;
                case 'A':
                    avance();
                    break;
            }
        }
    }

    @Override
    public String toString() {
        char directions[] = {'N', 'W', 'S', 'E'};
        return "Tondeuse{" +
                "positionX=" + positionX +
                ", positionY=" + positionY +
                ", directionActuelle=" + directions[Direction.volant.indexOf(this.directionActuelle)]+
                '}';
    }
}
