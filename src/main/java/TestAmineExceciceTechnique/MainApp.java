package TestAmineExceciceTechnique;

import TestAmineExceciceTechnique.Model.Direction;
import TestAmineExceciceTechnique.Model.Tondeuse;

/**
 * Created by Ouasmine on 27/11/2017.
 */
public class MainApp {

    /**
     * A main() so we can easily run these routing rules in our IDE
     */
    public static void main(String... args) throws Exception {
        Tondeuse tondeuse1 = new Tondeuse(1, 2, Direction.directionNorth);
        tondeuse1.executeInstructions("GAGAGAGAA");
        System.out.println(tondeuse1);

        Tondeuse tondeuse2 = new Tondeuse(3, 3, Direction.directionEast);
        tondeuse2.executeInstructions("AADAADADDA");
        System.out.println(tondeuse2);

    }

}

